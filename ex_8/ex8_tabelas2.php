<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=ISO 8859-1">
    <title>ex8</title>
  </head>
  <body>
  	<style>
	table {
	    border-collapse: collapse;
	    border-spacing: 0;
	    width: 75%;
	    border: 5px solid #eea5a7;
	}

	th, td {
	    text-align: center;
	    padding: 16px;
	}

	tr:nth-child(even) {
	    background-color: #add8e6
	}
	</style>

	<table>

      <tr>
        <th>Nome</th>
        <th>Idade</th>
        <th>N�mero de Aluno</th>
      </tr>

        <?php
          header("Content-Type: text/html; charset=ISO 8859-1",true);
          $servidor="localhost";
          $utilizador="root";
          $pass="#Qwerty3";
          $bd="teste";
          $ligacao=new mysqli($servidor, $utilizador, $pass, $bd);

          if($ligacao -> connect_error){
            die("Erro de conex�o". $ligacao -> connect_error);
        }

        $sql = "select * from alunos ";
        $res = $ligacao -> query($sql);

        if($res -> num_rows > 0){
          while($linha = $res -> fetch_assoc()){
        ?>
          <tr>
            <td><?php echo $linha["nome"]; ?></td>
            <td><?php echo $linha["idade"]; ?></td>
            <td><?php echo $linha["numaluno"]; ?></td>
          </tr>
      <?php }
      }
            $ligacao -> close();
      ?>
    </table>
  </body>

</html>